'use strict';

/* global cat, cd, cp, echo, exec, exit, find, ls, mkdir, pwd, rm, target, test */

require('shelljs/make');


target.compileFonts = function () {
    var fontsDir = 'cartridges/app_moravian_custom/cartridge/static/default/fonts';
    mkdir('-p', fontsDir);
    cp('-r', 'node_modules/font-awesome/fonts/', 'cartridges/app_moravian_custom/cartridge/static/default');
    cp('-r', 'node_modules/flag-icon-css/flags', fontsDir + '/flags');
};

target.compilePlugins = function () {
    // cp('-r', 'cartridges/app_moravian_custom/cartridge/client/default/js/plugins/', 'cartridges/app_moravian_custom/cartridge/static/default/js/');
};
